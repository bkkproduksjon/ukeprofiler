# Ukeprofilbygging
# NB! BKKs prognoser er i CET og allerede justert for sommertid i regnearket. Trenger derfor ikke å gjøre det her.

import pyodbc
import datetime
import numpy as np
import pandas as pd
import xlwt
from pandas import ExcelWriter
from pandas import ExcelFile

conn = pyodbc.connect(
    r'DRIVER={ODBC Driver 17 for SQL Server};'
    r'SERVER=;'
    r'UID=;'
    r'DATABASE=;'
    r'PWD='
    )

#OBS! Må ha Microsoft ODBC Driver for SQL Server installert på maskinen (evt. kan Native Client brukes hvis man har SQL SERVER installert)

# select query
cursor = conn.cursor()

#rapportTil = '20351231'
sistePrognoseDatoQ = """Select max(pr.[Fk Dim Tid Rapport Dato])
                    FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr
                    where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5"""
cursor.execute(sistePrognoseDatoQ)
r = cursor.fetchone()
sistePrognose = str(r[0])

datofraQ = """Select max(pr.[Rapport Dato])
                       FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr
                       where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5"""
cursor.execute(datofraQ)
r = cursor.fetchone()
datofra = r[0]

datotilQ = """Select max(pr.[Verdi Dato])
                FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr
                where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5 and pr.[Fk Dim Tid Rapport Dato] = """ + sistePrognose
cursor.execute(datotilQ)
r = cursor.fetchone()
datotil = r[0]

ukedagDatofra = datofra.isoweekday()
ukedagDatotil = datotil.isoweekday()

startDatoUkeQ = "select dateadd(ww, datediff(ww,0, (Select max(pr.[Rapport Dato]) FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5)),0)"
cursor.execute(startDatoUkeQ)
r = cursor.fetchone()
startDatoUke = r[0]

sluttdatoukeQ = "Select dateadd(ww, datediff(ww,0,(Select max(pr.[Rapport Dato]) FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5)),0)+6"
cursor.execute(sluttdatoukeQ)
r = cursor.fetchone()
sluttdatouke = r[0]

# henter data fra prognosen
sistePrognoseQ = """DECLARE @sisteprognose as NVARCHAR(8)
                    SET @sisteprognose = (Select max(pr.[Fk Dim Tid Rapport Dato]) FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny] pr where pr.[Fk Dim Kilde] = 1 and pr.[Fk Dim Variant] = 1 and pr.[Fk Dim Prisomraade] = 5)
                    SELECT [Verdi]
                    FROM [BKK_DWH].[markedsanalyse].[fakt_priskurver_ny]
                    where [Rapport Dato] = @sisteprognose and Kilde = 'BKK' and Beskrivelse = 'Full' and [Verdi Dato] >= @sisteprognose
                    order by [Verdi Dato] asc"""

cursor.execute(sistePrognoseQ)
rows = cursor.fetchall()
nprows = np.array(rows)
sistedata = []
for r in rows:
    sistedata.append(r[0])
npdata = np.array(sistedata)

#Finner antall timer å oppdatere i første uke:
fratime = (ukedagDatofra - 1) * 24 # evt justering på sommertid kunne settes her f.eks. + 2
posisjonerUke1 = [] # fylles med enere
timeprofilUker = []

#finner timeprofil ved ukegjennomsnitt
timerPerUke = 168
timerIgjenFrstUken = timerPerUke - fratime
for i in range(fratime):
    posisjonerUke1.append(1)

indices = np.arange(timerIgjenFrstUken,len(npdata)+1,timerPerUke)
indices = indices.tolist()
ukeprognoser = np.split(npdata, indices)
ukeprognoser = ukeprognoser[0:len(ukeprognoser)-1] #fjerner tom array på enden
for p in ukeprognoser:
    timeprofilUker.append(p/p.mean())   #regner ut timeprofiler

# konkatenerer 1-listen med profiler for resten av uke 1
timeprofilUker[0] = np.array(posisjonerUke1 + timeprofilUker[0].tolist())

# Må konvertere til list for å ta med strenger i radene
firstRow = ["NumberOfWeeks", "FromDate", "EndDate"]
for i in range(timerPerUke):
    firstRow.append(str(i+1))

timeprofil = []
ukeprofil = []
i = 0
for i in range(len(timeprofilUker)):
    ukeprofil.append(1)
    ukeprofil.append((startDatoUke + datetime.timedelta(days=7*i)).strftime("%d.%m.%Y"))
    ukeprofil.append((sluttdatouke + datetime.timedelta(days=7*i)).strftime("%d.%m.%Y"))
    ukeprofil = ukeprofil + timeprofilUker[i].tolist()
    timeprofil.append(ukeprofil)
    ukeprofil = []

#timeprofil.insert(0, firstRow)
df = pd.DataFrame(timeprofil, columns=firstRow)
writer = ExcelWriter('Ukeprofil_NO5_BKK_kort.xls')
df.to_excel(writer, 'Query', index=False)
writer.save()
